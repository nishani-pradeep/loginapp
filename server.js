const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const passport = require("passport");
const path = require("path");

const userRouter = require("./routes/api/users");


//This is the main server
const app = express();

app.use(
    bodyParser.urlencoded({
        extended: false
    })
);

app.use(bodyParser.json());

// DB Config
const db = require("./config/keys").MONGO_URI;

// Connect to MongoDB
mongoose.connect(db, { useNewUrlParser: true })
    .then(() => console.log("Mongo DB successfully connected"))
    .catch((err) => console.log("Error connecting MongoDB" + err));

//passport middleware
app.use(passport.initialize());
//Passport Config
require("./config/passport")(passport);


//Routes
app.use("/api/users", userRouter);

//Serve static assests if its in production
if (process.env.NODE_ENV === "production") {
    app.use(express.static("client/build"));

    app.get('*', (req, res) => {
        res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
    });
}

const port = process.env.PORT || 8090;

//server should listen to particular port
//and load web content on port hit
app.listen(port, () => console.log(`Server up and running on port ${port}`));
