**Login App**

This is a login app module consiting of both front ent and back end clubbed into a single application.
Tech Stack Used - MERN

- NodeJS
- ExpressJS
- React
- Mongo DB - > Gypsy
- mongoose
- PassportJS

## To Run
To run the app -> 
`npm run dev`

this will concurrently run client and backend together

- client : localhost:3000
- backend: localhost:5000


## Deployment
The app is currently deployed at heroku : https://frozen-oasis-81057.herokuapp.com/
Steps:

$ heroku login
Clone the repository
Use Git to clone frozen-oasis-81057's source code.


- $ git add .
- $ git commit -am "make it better"
- $ git push heroku master